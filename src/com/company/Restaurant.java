package com.company;

import java.util.*;

public class Restaurant {
    public static void main(String args[]) {
        Orders orders = new Orders();
        Map <String, Integer> listStock = new HashMap <String, Integer>();

        listStock.put( "tea", 7 );
        listStock.put( "coffee", 2 );
        listStock.put( "coolDrinks", 3 );
        listStock.put( "badam", 1 );
        listStock.put( "lemon", 2 );

        System.out.println( "Select your choice:" );
        Scanner scanner = new Scanner( System.in );
        Scanner scanner1 = new Scanner( System.in );
        Scanner scanner2 = new Scanner( System.in );
        int select = scanner.nextInt();

        do {
            switch (select) {
                case 1:

                    Set set = listStock.entrySet();
                    Iterator iterator = set.iterator();

                    while (iterator.hasNext()) {
                        Map.Entry entry = (Map.Entry) iterator.next();
                        System.out.println( entry.getKey() );
                    }

                    break;

                case 2:
                    orders.checkItems( listStock );
                    break;

                case 3:
                    System.out.println( "Items History:" );
                    int id = 0;
                    for (ArrayList <String> obj : orders.orderedListDetails) {
                        do {
                            id++;
                            System.out.print( id + " " );

                            for (String lists : obj) {

                                System.out.print( lists + " " );
                            }
                            System.out.println( "\n" );
                            break;
                        } while (id <= orders.orderedListDetails.size());
                    }
                    break;

                case 4:
                    System.out.println( "Choose waiter detail or Customer detail." );
                    int index = scanner2.nextInt();
                    if (index == 0 || index == 1) {
                        System.out.println( "Enter name :" );
                        String name = scanner1.nextLine();
                        System.out.println( name );
                        for (List <String> eachItem : orders.orderedListDetails) {
                            if (eachItem.get( index ).equals( name )) {
                                for (String items : eachItem) {
                                    System.out.print( items + " " );
                                }
                                System.out.println( "\n" );
                            }
                        }
                    }

                    break;
                case 5:
                    Set keyValue = listStock.entrySet();
                    Iterator iteration = keyValue.iterator();
                    while (iteration.hasNext()) {
                        Map.Entry entry = (Map.Entry) iteration.next();
                        System.out.println( entry.getKey() + ":" + entry.getValue() );
                    }
            }
            System.out.println( "Select your choice:" );
            select = scanner.nextInt();

        } while (select < 6);

    }
}