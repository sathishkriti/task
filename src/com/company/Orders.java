package com.company;

import java.util.*;

public class Orders {

    Scanner scanner1 = new Scanner( System.in );
    String order;
    String customerName;
    String waiterName;
    List <ArrayList <String>> orderedListDetails = new ArrayList <ArrayList <String>>();


    public void checkItems(Map <String, Integer> listStock) {
        ArrayList <String> orderDetails = new ArrayList <>();

        System.out.println( "Ordered Item:" );
        order = scanner1.nextLine();


        if (listStock.get( order ) != null) {
            System.out.println( "Customer Name:" );
            customerName = scanner1.nextLine();

            System.out.println( "Waiter Name:" );
            waiterName = scanner1.nextLine();

            int numberofitems = listStock.get( order ) - 1;
            listStock.put( order, numberofitems );

            orderDetails.add( customerName );
            orderDetails.add( waiterName );
            orderDetails.add( order );

            orderedListDetails.add( orderDetails );
        } else
            System.out.println( "The given Item not Available" );
    }
}